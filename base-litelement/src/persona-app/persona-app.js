import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer';
import '../persona-sidebar/persona-sidebar';

class PersonaApp extends LitElement {

    static get properties() {
        return {};
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <persona-header></persona-header>
            <div class="container">
                <div class="row" >
                    <persona-sidebar @new-person="${this.newPerson}" class="col-md-2"></persona-sidebar>
                    <persona-main class="col-md-10"></persona-main>
                </div>
            </div>
            <persona-footer></persona-footer>
        `;
    }

    newPerson(e){
        console.log("newPerson en persona-app");
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }    
}

customElements.define('persona-app', PersonaApp)