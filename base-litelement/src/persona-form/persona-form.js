import { LitElement, html } from 'lit-element';

export class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: { type: Object }
        };
    }

    constructor() {
        super();
        this.person = {};
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
                integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="col-md">
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input @input="${this.updateName}" type="text" id="personFormName" class="form-control"
                            placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" id="personFormProfile" class="form-control" placeholder="Perfil"
                            rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsinCompany}" type="text" id="personFormYearsInCompany" class="form-control"
                            placeholder="Años en la empresa" />
                    </div>
                    <div class="form-row">
                        <div class="col-md">
                            <button @click="${this.goBack}" class="btn btn-primary btn-lg btn-block"><strong>Atrás</strong></button>
                        </div>
                        <div class="col-md">
                            <button @click="${this.storePerson}" class="btn btn-success btn-lg btn-block"><strong>Guardar</strong></button>
                        </div>                                        
                    </div>
                </form>
                <div id="msgError" style="color: red; font-weight: bold; font-size: 30px; padding-top: 30px"></div>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
        this.cleanInputForms();
    }

    storePerson(e) {
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src": "./img/goku_profile.jpg",
            "alt": "Persona"
        }

        console.log("La propiedad name vale: " + this.person.name);
        console.log("La propiedad profile vale: " + this.person.profile);
        console.log("La propiedad yearsInCompany vale: " + this.person.yearsInCompany);

        if(this.person.name && this.person.profile && this.person.yearsInCompany){
            if(!isNaN(this.person.yearsInCompany)){
                this.dispatchEvent(new CustomEvent("persona-form-store", {
                    detail: {
                        person: {
                            name: this.person.name,
                            profile: this.person.profile,
                            yearsInCompany: this.person.yearsInCompany,
                            photo: this.person.photo
                        }
                    }
                }));
                this.person = {};
                this.cleanInputForms();
            }else{
                this.shadowRoot.getElementById("msgError").innerHTML = "";
                var newMsg = document.createElement("div");
                newMsg.innerHTML = "El campo de años en la empresa es NUMERICO";
                this.shadowRoot.getElementById("msgError").appendChild(newMsg); 
            }
        }else{            
            this.shadowRoot.getElementById("msgError").innerHTML = "";
            var newMsg = document.createElement("div");
            newMsg.innerHTML = "Es necesario llenar todos los campos.";
            this.shadowRoot.getElementById("msgError").appendChild(newMsg);
        }
    }

    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor: " + e.target.value);
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        console.log("updateProfile");
        console.log("Actualizando la propiedad  profile con el valor: " + e.target.value);
        this.person.profile = e.target.value;
    }

    updateYearsinCompany(e) {
        console.log("updateYearsinCompany");
        console.log("Actualizando la propiedad yearsincompany con el valor: " + e.target.value);
        this.person.yearsInCompany = e.target.value;
    }

    cleanInputForms() {
        console.log("cleanInputForms");
        console.log("Limpiando el valor de los inputs");
        this.shadowRoot.getElementById("personFormName").value = "";
        this.shadowRoot.getElementById("personFormProfile").value = "";
        this.shadowRoot.getElementById("personFormYearsInCompany").value = "";
        this.shadowRoot.getElementById("msgError").innerHTML = "";
    }
}
customElements.define('persona-form', PersonaForm);